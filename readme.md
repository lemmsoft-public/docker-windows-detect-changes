![npm](https://img.shields.io/npm/v/docker-windows-detect-changes.svg)
![node](https://img.shields.io/node/v/docker-windows-detect-changes.svg)
![pipeline](https://gitlab.com/lemmsoft-public/docker-windows-detect-changes/badges/master/pipeline.svg)
[![Coverage Status](https://coveralls.io/repos/gitlab/lemmsoft-public/docker-windows-detect-changes/badge.svg?branch=master)](https://coveralls.io/gitlab/lemmsoft-public/docker-windows-detect-changes?branch=master)
![npm](https://img.shields.io/npm/dt/docker-windows-detect-changes.svg)
![npm](https://img.shields.io/npm/dm/docker-windows-detect-changes.svg)
<br/>

# Docker Windows Detect Changes
A small tool meant to help with file change detection within Linux containers on Docker for Windows.<br/>
How it works:<br/>
1. An API server is started within the Docker container
2. A file watcher is started in the Windows powershell.
3. The file watcher detects changes and sends an HTTP request to the server.
4. The server performs a "touch" on a file set in the config, which propagates the change detection within the container.

## Table Of Contents
- [Getting Started](#getting-started)
- [Config File Reference](#config-file-reference)
- [Full CLI Reference](#full-cli-reference)

# Getting Started
First, you'll need to install docker-windows-detect-changes both in Windows (outside the Docker container) and inside the Docker container.
```
npm i @lemmsoft/docker-windows-detect changes
```

If you don't want to install NodeJS and NPM in Windows itself, simply download the latest binary from the <a href='https://gitlab.com/lemmsoft-public/docker-windows-detect-changes/-/tree/master/build'>build</a> folder.

<br/>
Now you'll need to create a file named "docker-windows-detect-changes.json" in the root directory of your project:

```json
{
	"port": 1234,
	"paths": [
		{
			"fileToTouch": "./back-end/main.js",
			"ignoredRegexString": "./back-end/main.js",
			"pathToWatch": "./back-end"
		}
	]
}
```
The above configuration will start a change detection API server at port 1234, and will set up the change detection watcher to touch the file "./back-end/main.js" whenever something in the "./back-end" directory and its subdirectories is added/changed/deleted.
<br/><br/>
Then, in the docker container, go to the directory containing the file you just created and start the change detection API server by running

```
docker-windows-detect-changes -s
```
This will start the server at port 1234.
<br/><br/>
After the server is started, outside the container - in Windows - navigate to the directory containging the .json config file and run

```
docker-windows-detect-changes
```
This will start the file watcher for the "pathToWatch" directory.
<br/><br/>
That's it - you're good to go!
<br/>
<br/>
<br/>



# Config File Reference
```ts
// docker-windows-detect-changes.json
{
	"port": number, // the port that the server will be started on; required if using the -s flag when running the docker-windows-detect-changes command
	// the configuration for the paths that are to be watched; required when running the docker-windows-detect-changes command without the -s flag
	"paths": [
		{
			"fileToTouch": string, // the file that will be touched when a change is detected, which will in turn propagate the changes inside the docker container
			"ignoredRegexString": string | RegExp, // a string or regular expression, matching the files or folders to be excluded from the file watcher; must contain the "fileToTouch" file, otherwise an infinite loop will be created
			"pathToWatch": string // the path to the file or folder to be watched
		}
	]
}
```
<br/>
<br/>
<br/>

# Full CLI Reference
Usage: docker-windows-detect-changes [options]<br/>
<br/>
Options:<br/>
&nbsp;&nbsp;-s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;starts a ChangeDetectionServer at the TCP port provided using the -p argument <br/>
&nbsp;&nbsp;-p&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;required and only used if -s is provided; specifies the TCP port at which the ChangeDetectionServer should be started<br/>
&nbsp;&nbsp;-c&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;optional; specifies the config file to use for starting the ChangeDetectionWatcher; if not provided, the script will look for a file named "docker-windows-detect-changes.json" in the process' working directory<br/>
&nbsp;&nbsp;-h, --help&nbsp;&nbsp;&nbsp;display the help menu