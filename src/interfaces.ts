export interface IChangeDetectorConfig {
	paths: IChangeDetectorPathDataConfig[]
	port: number
	verbose?: boolean
}

export interface IChangeDetectorPathDataConfig {
	fileToTouch: string
	ignoredRegexString?: string
	pathToWatch: string
}
