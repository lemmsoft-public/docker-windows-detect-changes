import * as chokidar from 'chokidar'
import {CustomError} from '@ramster/general-tools'
import {IChangeDetectorConfig} from '../interfaces'
import * as fetch from 'isomorphic-fetch'

/**
 * Contains the logic for watching a directory for changes and triggering the appropriate on-change events.
 */
export class ChangeDetectionWatcher {
	addEventsPerIntervalTick: {
		[timestamp: string]: {
			currentTick: number
			initialWatchingSetupWrappedUp: boolean
			pathIndex: number
			ticks: {[tick: string]: number}
			watcher: chokidar.FSWatcher
		}
	} = {}
	loggerMethod: Function

	/**
	 * @param config (required) - The change detector config, structured according to the IChangeDetectorConfig interface.
	 */
	constructor(public config: IChangeDetectorConfig) {
		this.loggerMethod = config.verbose ? function() { console.log.apply(console, arguments as any) } : () => true
	}

	/**
	 * Checks the number of watcher events for the current file & interval tick, and increments the number in question by 1. Also triggers the change method for new files.
	 * @param timestamp (required) - The unique timestamp, generated when the watch method was called for the particular path.
	 * @returns void
	 */
	callTheChangeDetectionAPI(pathIndex: number, event: string): Promise<void> {
		let instance = this
		return new Promise((resolve, reject) => {
			const pathConfig = instance.config.paths[pathIndex]
			instance.loggerMethod('[docker-windows-detect-changes]: event triggered:', event)
			fetch(`http://127.0.0.1:${instance.config.port}/triggerChange/${encodeURIComponent(pathConfig.fileToTouch)}`).then(
				(response) => {
					if (response.status >= 400) {
						instance.loggerMethod('[docker-windows-detect-changes]: triggering change for event:', event, '; error:', response)
						reject(response)
					}
					instance.loggerMethod('[docker-windows-detect-changes]: event change trigger sent successfully:', event)
					resolve()
				}
			)
		})
	}

	/**
	 * Starts a script that watches a directory for changes and calls the change detection server's triggerChange api endpoint for a set file whenever there's a change.
	 * @param pathIndex (required) - The path from the array of paths that the watcher must be started for.
	 * @returns Promise<void>
	 */
	watch(pathIndex: number): Promise<void> {
		let instance = this
		return new Promise((resolve, reject) => {
			const pathConfig = instance.config.paths[pathIndex]
			if (!pathConfig) {
				reject(new CustomError(`Path config at index ${pathIndex} not found.`))
			}
			const watcher = chokidar.watch(
				pathConfig.pathToWatch,
				{
					ignored: pathConfig.ignoredRegexString ? new RegExp(pathConfig.ignoredRegexString) : pathConfig.fileToTouch,
					persistent: true
				}
			)
			let now = (new Date()).valueOf()
			instance.addEventsPerIntervalTick[now] = {
				currentTick: 0,
				initialWatchingSetupWrappedUp: false,
				pathIndex,
				ticks: {},
				watcher
			}
			const interval = setInterval(
				() => {
					let {currentTick, ticks} = instance.addEventsPerIntervalTick[now]
					if (
						(currentTick < 2) ||
						(ticks[currentTick] > 0) ||
						(ticks[currentTick - 1] > 0) ||
						(ticks[currentTick - 2] > 0)
					) {
						instance.addEventsPerIntervalTick[now].currentTick++
						return
					}
					clearInterval(interval)
					instance.addEventsPerIntervalTick[now].initialWatchingSetupWrappedUp = true
					watcher.on('all', instance.callTheChangeDetectionAPI.bind(instance, pathIndex))
					instance.loggerMethod('[docker-windows-detect-changes]: watch started for paths:', watcher.getWatched())
					resolve()
				},
				50
			)
			watcher.on('add', instance.watcherInitializationChecker.bind(instance, now))
			watcher.on('addDir', instance.watcherInitializationChecker.bind(instance, now))
		})
	}

	/**
	 * Checks the number of watcher events for the current file & interval tick, and increments the number in question by 1. Also triggers the change method for new files.
	 * @param timestamp (required) - The unique timestamp, generated when the watch method was called for the particular path.
	 * @returns void
	 */
	watcherInitializationChecker(timestamp: number): void {
		let currentData = this.addEventsPerIntervalTick[timestamp]
		if (currentData.initialWatchingSetupWrappedUp) {
			currentData.watcher.on('all', this.callTheChangeDetectionAPI.bind(this, currentData.pathIndex))
			return
		}
		if (!currentData.ticks[currentData.currentTick]) {
			currentData.ticks[currentData.currentTick] = 0
		}
		currentData.ticks[currentData.currentTick]++
	}
}
