import {ChangeDetectionWatcher, IChangeDetectorConfig} from '../../dist'
import {exec} from 'child_process'
import * as fs from 'fs-extra'
import * as path from 'path'
import {strict as assert} from 'assert'

describe('ChangeDetectionWatcher', function() {
	const config: IChangeDetectorConfig = {
		paths: [
			{
				fileToTouch: __filename,
				ignoredRegexString: '/ignoredDirectory/',
				pathToWatch: __dirname
			},
			{
				fileToTouch: path.join(__filename, '/ignoredDirectory/foo/bar/baz'),
				pathToWatch: path.join(__filename, '/ignoredDirectory')
			}
		],
		port: 6601,
		verbose: true
	}
	let changeDetectionWatcher: ChangeDetectionWatcher | null = null
	before(function() {
		this.timeout(10000)
		return (async function() {
			await fs.mkdirp(path.join(__dirname, '/ignoredDirectory'))
			await new Promise((resolve, reject) => {
				exec(`touch ${path.join(__dirname, '/ignoredDirectory/watchedFile')}`, ((err, _stdout, stderr) => {
					const error = err || stderr || null
					if (error) {
						reject(error)
					}
					resolve()
				}))
			})
			changeDetectionWatcher = new ChangeDetectionWatcher(config)
			changeDetectionWatcher.watch(0)
			changeDetectionWatcher.watch(1)
			new ChangeDetectionWatcher(Object.assign(config, {verbose: false}))
			await new Promise((resolve) => {
				setTimeout(() => resolve(), 1000)
			})
		})()
	})
	describe('callTheChangeDetectionAPI', function() {
		it('should throw an error with the correct message if an error is thrown by the server', function() {
			return (async function() {
				let didThrowAnError = false
				try {
					await changeDetectionWatcher!.callTheChangeDetectionAPI(1, 'change')
				}
				catch(e) {
					didThrowAnError = true
				}
				assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected ${true}`)
			})()
		})
		it(`should execute the watch method successfully and call the changeDetectionServer's triggerChange method if a file change is registered`, function() {
			return (async function() {
				await changeDetectionWatcher!.callTheChangeDetectionAPI(0, 'change')
			})
		})
	})
	describe('watch', function() {
		it('should throw an error with the correct message if a config at the provided index does not exist', function() {
			return (async function() {
				const message = 'Path config at index 2 not found.'
				let didThrowAnError = false
				try {
					await changeDetectionWatcher!.watch(2)
				}
				catch(e) {
					didThrowAnError = true
					assert.strictEqual(e.name, 'CustomError', `bad value ${e.name} for e.name, expected ${'CustomError'}`)
					assert.strictEqual(e.message, message, `bad value ${e.message} for e.message, expected ${message}`)
				}
				assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected ${true}`)
			})()
		})
		it(`should execute the watch method successfully and call the changeDetectionServer's triggerChange method if a file change is registered on a new file, then on a previously watched file`, function() {
			return (async function() {
				const newFileName = path.join(__dirname, (new Date()).valueOf().toString())
				await new Promise((resolve, reject) => {
					exec(
						`touch ${newFileName}`,
						((err, _stdout, stderr) => {
							const error = err || stderr || null
							if (error) {
								reject(error)
							}
							resolve()
						})
					)
				})
				// await new Promise((resolve, reject) => {
				// 	exec(
				// 		`touch ${newFileName}`,
				// 		((err, _stdout, stderr) => {
				// 			const error = err || stderr || null
				// 			if (error) {
				// 				reject(error)
				// 			}
				// 			resolve()
				// 		})
				// 	)
				// })
				await new Promise((resolve, reject) => {
					exec(
						`touch ${path.join(__filename)}`,
						((err, _stdout, stderr) => {
							const error = err || stderr || null
							if (error) {
								reject(error)
							}
							resolve()
						})
					)
				})
			})()
		})
		it(`should call the changeDetectionServer's triggerChange method and handle the returned error if a file change to a non-existing file is trigger`, function() {
			this.timeout(10000)
			return (async function() {
				await new Promise((resolve, reject) => {
					exec(
						`touch ${path.join(__dirname, '/ignoredDirectory/watchedFile')}`,
						((err, _stdout, stderr) => {
							const error = err || stderr || null
							if (error) {
								reject(error)
							}
							resolve()
						})
					)
				})
			})()
		})
	})
	describe('watcherInitializationChecker', function() {
		let timestamp = 0
		before(function() {
			timestamp = parseInt(Object.keys(changeDetectionWatcher!.addEventsPerIntervalTick)[0], 10)
			changeDetectionWatcher!.addEventsPerIntervalTick[timestamp].initialWatchingSetupWrappedUp = false
		})
		it(`should execute successfully and increment the number of ticks for a timestamp if the initial setup for the timestamp hasn't been completed`, function() {
			changeDetectionWatcher!.watcherInitializationChecker(timestamp)
		})
		it(`should execute successfully and bind the onChange event for a timestamp if the initial setup for the timestamp has been completed`, function() {
			changeDetectionWatcher!.addEventsPerIntervalTick[timestamp].initialWatchingSetupWrappedUp = true
			changeDetectionWatcher!.watcherInitializationChecker(timestamp)
		})
	})
	after(function() {
		this.timeout(10000)
		return (async function() {
			const watchedPathsData = changeDetectionWatcher!.addEventsPerIntervalTick
			for (const timestamp in watchedPathsData) {
				await watchedPathsData[timestamp].watcher.close()
			}
		})()
	})
})
