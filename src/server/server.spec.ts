import {ChangeDetectionServer} from '../../dist'
import * as fetch from 'isomorphic-fetch'
import {strict as assert} from 'assert'

describe('ChangeDetectionServer', function() {
	const config = {port: 6601}
	let changeDetectionServer: ChangeDetectionServer | null = null
	before(function() {
		changeDetectionServer = new ChangeDetectionServer(config)
	})
	describe('listen', function() {
		it('should execute the listen method successfully and start the change detection server at the provided port, with the triggerChange method mounted correctly', function() {
			return (async function() {
				await changeDetectionServer!.listen()
				let response = await fetch(`http://127.0.0.1:${config.port}/triggerChange/${encodeURIComponent(__filename)}`)
				assert.strictEqual(response.status, 200, `bad value ${response.status} for response.status, expected ${200}`)
			})()
		})
	})
	describe('triggerChange', function() {
		it('should throw an error if the file does not exist', function() {
			return (async function() {
				const fakeFileName = __dirname + '/foo/bar/baz_' + (new Date()).valueOf().toString()
				let response = await fetch(
					`http://127.0.0.1:${config.port}/triggerChange/${encodeURIComponent(fakeFileName)}`
				)
				assert.strictEqual(response.status, 500, `bad value ${response.status} for response.status, expected ${500}`)
			})()
		})
	})
})
