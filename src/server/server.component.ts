import * as express from 'express'
import {exec} from 'child_process'
import * as http from 'http'

/**
 * Contains the logic for starting a webserver that watches for change events emitted as HTTP requests by the watcher script. The api endpoint provided by the server triggers a touch event 
 */
export class ChangeDetectionServer {
	/**
	 * @param config (required) - An object, containing the port, which the server must be started on.
	 */
	constructor(
		public config: {port: number}
	) {
	}

	/**
	 * Mounts the server routes.
	 * @returns Promise<boolean>
	 */
	listen(): Promise<boolean> {
		const instance = this;
		return (async function() {
			let app = express()
			app.get(
				'/triggerChange/:pathToFile',
				instance.triggerChange()
			)
			await new Promise((resolve) => {
				http.createServer(app).listen(instance.config.port, () => {
					console.log(`[docker-windows-detect-changes]: server started on port ${instance.config.port}`)
					resolve(true)
				})
			})
			return true
		})()
	}

	/**
	 * Returns an express.RequestHandler middleware that accepts a file path uri paramter and triggers a touch event for that file.
	 * @returns express.RequestHandler
	 */
	triggerChange(): express.RequestHandler {
		return async function(req: express.Request, res: express.Response) {
			try {
				const command = `touch ${decodeURIComponent(req.params.pathToFile)}`
				console.log(`[docker-windows-detect-changes]: ${command}`)
				await new Promise((resolve, reject) => {
					exec(command, ((err, _stdout, stderr) => {
						const error = err || stderr || null
						if (error) {
							reject(error)
						}
						console.log(`[docker-windows-detect-changes]: touch closed`)
						resolve(true)
					}))
				})
				res.status(200).end()
			}
			catch (e) {
				console.log('[docker-windows-detect-changes]: touch error:', e)
				res.status(500).end()
			}
		}
	}
}
