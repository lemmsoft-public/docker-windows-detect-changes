// import {ChangeDetectionServer} from '../../dist'
import * as child_process from 'child_process'
import * as fetch from 'isomorphic-fetch'
import * as fs from 'fs-extra'
import * as path from 'path'
import {promisify} from 'util'
import {strict as assert} from 'assert'

const exec = promisify(child_process.exec),
	nycPath = path.join(__dirname, '../../node_modules/.bin/nyc'),
	spawnPromise = (command: string, args: string[]): Promise<void> => new Promise((resolve, reject) => {
		const proc = child_process.spawn(command, args)
		let errors = ''
		proc.stderr.on('data', (data) => errors += data.toString())
		proc.stdout.on('data', (data) => {
			const message = data.toString()
			console.log(message)
			if (message.match(/success/g)) {
				resolve()
			}
		})
		proc.on('close', (code) => {
			if (code) {
				reject(new Error(errors))
			}
			resolve()
		})
	}),
	killServer = async function(port: number) {
		let data = await exec(`netstat -tulpn | grep ${port}`)
		if (data.stderr && data.stderr.length) {
			throw new Error(data.stderr)
		}
		let matches = data.stdout.match(/(\s|\t)(\d+)\/node/)
		if (!matches) {
			return
		}
		data = await exec(`kill ${matches[2]}`)
		if (data.stderr && data.stderr.length) {
			throw new Error(data.stderr)
		}
	}

describe('CLI', function() {
	before(function() {
		this.timeout(10000)
		return (async function() {
			try {
				await killServer(6602)
			}
			catch(e) {
				console.log(e)
			}
			try {
				await killServer(6603)
			}
			catch(e) {
				console.log(e)
			}
			try {
				await killServer(6604)
			}
			catch(e) {
				console.log(e)
			}
		})()
	})
	it('should execute successfully and return the help information if the help flag is provided', function() {
		return (async function() {
			let data = await exec(`${nycPath} --silent node ${path.join(__dirname, '../../dist/cli/cli.js')} -h`)
			if (data.stderr && data.stderr.length) {
				throw new Error(data.stderr)
			}
			assert(data.stdout.match(/^Usage:\sdocker-windows-detect-changes/), `bad value ${data.stdout} for data.stdout`)
		})()
	})
	it('should execute successfully and start a ChangeDetectionServer at the correct port if the -s flag is provided and a correct -p argument is provided', function() {
		this.timeout(10000)
		return (async function() {
			await spawnPromise(nycPath, ['--silent', 'node', path.join(__dirname, '../../dist/cli/cli.js'), '-s', '-p', '6602'])
			let response = await fetch(`http://127.0.0.1:${6602}/triggerChange/${encodeURIComponent(__filename)}`)
			await killServer(6602)
			assert.strictEqual(response.status, 200, `bad value ${response.status} for response.status, expected ${200}`)
		})()
	})
	it('should execute successfully and start a ChangeDetectionServer at the correct port if the -s flag is provided, no -c argument is provided and a docker-windows-detect-changes.json file containing the correct config exists in the current working directory', function() {
		this.timeout(10000)
		return (async function() {
			await spawnPromise(nycPath, ['--silent', 'node', path.join(__dirname, '../../dist/cli/cli.js'), '-s'])
			let response = await fetch(`http://127.0.0.1:${6603}/triggerChange/${encodeURIComponent(__filename)}`)
			assert.strictEqual(response.status, 200, `bad value ${response.status} for response.status, expected ${200}`)
		})()
	})
	it('should execute successfully and start a ChangeDetectionServer at the correct port if the -s flag is provided, a -c argument is provided and a docker-windows-detect-changes.json file containing the correct config exists in the provided directory', function() {
		this.timeout(10000)
		return (async function() {
			await spawnPromise(nycPath, ['--silent', 'node', path.join(__dirname, '../../dist/cli/cli.js'), '-s', '-c', './test/docker-windows-detect-changes2.json'])
			let response = await fetch(`http://127.0.0.1:${6604}/triggerChange/${encodeURIComponent(__filename)}`)
			assert.strictEqual(response.status, 200, `bad value ${response.status} for response.status, expected ${200}`)
		})()
	})
	it('should execute successfully and throw an error if the -s flag is not provided, no -c argument is provided and a docker-windows-detect-changes.json file not containing any paths exists in the current working directory', function() {
		this.timeout(10000)
		return (async function() {
			let didThrowAnError = false
			try {
				await spawnPromise(nycPath, ['--silent', 'node', path.join(__dirname, '../../dist/cli/cli.js')])
			}
			catch(e) {
				console.log(e)
				didThrowAnError = true
			}
			assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected ${true}`)
		})()
	})
	it('should execute successfully and start a ChangeDetectionServer at the correct port if the -s flag is not provided, no -c argument is provided and a docker-windows-detect-changes.json file containing the correct config exists in the current working directory', function() {
		this.timeout(10000)
		return (async function() {
			let fd = await fs.open(path.join(__dirname, '../../docker-windows-detect-changes.json'), 'w')
			await fs.writeFile(
				fd,
				JSON.stringify({
					"port": 6603,
					"paths": [
						{
							"fileToTouch": "./test/main.js",
							"ignoredRegexString": "./test/cli",
							"pathToWatch": "./test"
						},
						{
							"fileToTouch": "./test/foo/bar/baz",
							"pathToWatch": "./test"
						}
					]
				})
			)
			await fs.close(fd)
			await spawnPromise(nycPath, ['--silent', 'node', path.join(__dirname, '../../dist/cli/cli.js')])
		})()
	})
	it('should execute successfully and start a ChangeDetectionServer at the correct port if the -s flag is not provided, a -c argument is provided and a docker-windows-detect-changes.json file containing the correct config exists in the provided directory', function() {
		this.timeout(10000)
		return (async function() {
			let fd = await fs.open(path.join(__dirname, '../../test/docker-windows-detect-changes2.json'), 'w')
			await fs.writeFile(
				fd,
				JSON.stringify({
					"port": 6604,
					"paths": [
						{
							"fileToTouch": "./test/main.js",
							"ignoredRegexString": "./test/cli",
							"pathToWatch": "./test"
						},
						{
							"fileToTouch": "./test/foo/bar/baz",
							"pathToWatch": "./test"
						}
					]
				})
			)
			await fs.close(fd)
			await spawnPromise(nycPath, ['--silent', 'node', path.join(__dirname, '../../dist/cli/cli.js'), '-c', './test/docker-windows-detect-changes2.json'])
		})()
	})
	after(function() {
		this.timeout(11000)
		return (async function() {
			try {
				await killServer(6602)
			}
			catch(e) {
				console.log(e)
			}
			try {
				await killServer(6603)
			}
			catch(e) {
				console.log(e)
			}
			try {
				await killServer(6604)
			}
			catch(e) {
				console.log(e)
			}
			let fd = await fs.open(path.join(__dirname, '../../docker-windows-detect-changes.json'), 'w')
			await fs.writeFile(
				fd,
				JSON.stringify({
					"port": 6603
				})
			)
			await fs.close(fd)
			fd = await fs.open(path.join(__dirname, '../../test/docker-windows-detect-changes2.json'), 'w')
			await fs.writeFile(
				fd,
				JSON.stringify({
					"port": 6604
				})
			)
			await fs.close(fd)
			setTimeout(
				() => {
					process.exit(0)
				},
				1000
			)
		})()
	})
})
