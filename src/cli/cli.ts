import {argv} from 'yargs'
import {
	ChangeDetectionServer,
	ChangeDetectionWatcher,
	IChangeDetectorConfig
} from '../index'
import * as path from 'path'

if (argv.h || argv.help) {
	console.log(
		`Usage: docker-windows-detect-changes [options]\n` +
		`\n` +
		`Options:\n` +
		`  -s             starts a ChangeDetectionServer at the TCP port provided using the -p argument \n` +
		`  -p             required and only used if -s is provided; specifies the TCP port at which the ChangeDetectionServer should be started\n` +
		`  -c             optional; specifies the config file to use for starting the ChangeDetectionWatcher; if not provided, the script will look for a file named "docker-windows-detect-changes.json" in the process' working directory\n` +
		`  -h, --help     display this menu\n`
	)
	process.exit(0)
}

(async function() {
	const configPath = argv.c ? path.resolve(argv.c as string) : path.join(process.cwd(), 'docker-windows-detect-changes.json')
	if (argv.s) {
		let port = 0
		if (argv.p) {
			port = parseInt(argv.p as string, 10)
		}
		else {
			const config = (await import(configPath)) as IChangeDetectorConfig
			port = config.port
		}
		const server = new ChangeDetectionServer({port})
		await server.listen()
		return
	}
	const config = (await import(configPath)) as IChangeDetectorConfig,
		{paths} = config,
		watcher = new ChangeDetectionWatcher(config)
	// console.log('====>', config)
	for (let i = 0; i < paths.length; i++) {
		// console.log('====> start', i)
		await watcher.watch(i)
		// console.log('====> end', i)
	}
	// console.log('====> done')
})().then(
	() => {
		console.log('success')
	},
	(err) => {
		console.error(err)
		process.exit(1)
	}
)
