# 1.1.1
- Readme fix.

# 1.1.0
- Added a platform-based build and updated the readme

# 1.0.0
- Initial version.
